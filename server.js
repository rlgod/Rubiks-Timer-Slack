var http = require("http");
var mongoose = require("mongoose");
var moment = require("moment");
var bodyParser = require("body-parser");
var multer = require('multer');
var express = require("express");
var validator = require("validator");
var pHasher = require("password-hash");
var Slack = require('slack-node');
var slack = new Slack();
slack.setWebhook("https://hooks.slack.com/services/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");

var helpString = '*Usage*: rubiks <command>\n' +
                 '*Commands*: \n' +
                 '_scoreboard_: Overall time leaderboard\n' +
                 '_average <email>_: Get average for user with email address\n' +
                 '_users_: Get a list of users and emails';

var uristring =
    process.env.MONGOLAB_URI ||
    process.env.MONGOHQ_URL ||
    'mongodb://localhost:27017/rubiks';

var port = process.env.PORT || 3000;

mongoose.connect(uristring, function(err, res){
  if (err) {
    console.log("Error connecting to: " + uristring + '. ' + err);
  } else {
    console.log("Succeeded connecting to: " + uristring);
  }
});

var userSchema = new mongoose.Schema({
  email: String,
  password: String,
  firstname: String,
  lastname: String,
  attempts: [{
    startTime: { type: Date, default: Date.now },
    endTime: { type: Date }
  }]
});

var userModel = mongoose.model('User', userSchema);

var app = express();
var upload = multer();
var apiRouter = express.Router();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

function sendSlackAttempt(user, attempt) {
  var duration = moment(Date.parse(attempt.endTime) - Date.parse(attempt.startTime));

  slack.webhook({
    channel: '#cubetalk',
    username: 'cube-timer',
    text: user.firstname + ' ' + user.lastname + ' just recorded a new solve time of '
    + duration.format("mm:ss:SS")
  }, function(err, response){
    if (err)
      console.log(err);
  });
}

apiRouter.post('/slack', upload.array(), function(req, res){
  var command = req.body.text.split(' ');

  switch(command[1]) {
    case("help"):
      res.send({text: helpString});
      break;
    case("users"):
      users(req, res);
      break;
    case("scoreboard"):
      scoreboard(req, res);
      break;
    case("average"):
      average(req, res, command);
      break;
    default:
      break;
  }
});

function users(req, res){
  userModel.find({}, 'email firstname lastname', function(err, users){
    if (!err){
      var content = "";
      for(var i = 0; i < users.length; i++) {
        content += users[i].email + ', ' + users[i].firstname + ', ' + users[i].lastname + '\n';
      }
      res.send({text: content});
    } else {
      console.log(err);
    }
  });
}

function average(req, res){
  var commandText = req.body.text.split(" ");
  var email = commandText[2].split("|")[1].split(">")[0];
  if (email !== null) {
    userModel.findOne({email: email}, function(err, user){
      if (!err) {
        if (user === null) {
          res.send({text: "User: " + email + " not found"});
          return;
        }
        var n = user.attempts.length;
        var total = 0;
        var average;

        for (var i = 0; i < n; i++){
          total += user.attempts[i].endTime.getTime() - user.attempts[i].startTime.getTime();
        }

        average = total/n;
        res.send({text: "Average time for " + email + " is " + moment(average).format("mm:ss:SS")});
      } else {
        console.log(err);
      }
    });
  } else {
    res.send({text: "You must supply an email address\n\n" + helpString});
  }
}

function scoreboard(req, res){
  var o = {};

  var mapFunc = function() {
    var key = {
      email: this.email,
      firstname: this.firstname,
      lastname: this.lastname
    }

    for (var i = 0; i < this.attempts.length; i++) {
      emit(key, this.attempts[i].endTime.getTime() - this.attempts[i].startTime.getTime());
    }
  }

  var reduceFunc = function(key, times) {
    var bestTimeMS = null;
    for (var i = 0; i < times.length; i++) {
      if (bestTimeMS === null || times[i] < bestTimeMS) {
        bestTimeMS = times[i];
      }
    }
    return bestTimeMS;
  }

  o.map = mapFunc;
  o.reduce = reduceFunc;

  userModel.mapReduce(o, function(err, results){
    if (!err){
      var content = "";
      results.sort(function(a, b){
        if (a.value == b.value){
          return 0;
        }
        return a.value < b.value ? -1 : 1;
      });
      for (var i = 0; i < results.length; i++) {
        content = content + results[i]._id.firstname + ' ' + results[i]._id.lastname + ' - ' + moment(results[i].value).format("mm:ss:SS") + '\n';
      }
      res.send({text: content});
    } else {
      console.log(err);
      res.status(500).send({ error: err });
    }
  });
}

apiRouter.post('/attempt', upload.array(), function(req, res){
  if (validator.isEmail(req.body.email) &&
     !validator.isNull(req.body.password) &&
     validator.isDate(req.body.startTime) &&
     validator.isDate(req.body.endTime)) {
    var query = userModel.where({ email: req.body.email });

    query.findOne(function(err, user){
      if (!err) {
        if (user !== null) {
          if (pHasher.verify(req.body.password, user.password)) {
            var user = new userModel({
              email: req.body.email,
              password: req.body.password
            });

            var startTime = Date.parse(req.body.startTime);
            var endTime = Date.parse(req.body.endTime);

            query.findOneAndUpdate({ email: req.body.email }, { '$push': { attempts: { startTime: startTime, endTime: endTime } } }, function(err, user){
              if (!err) {
                sendSlackAttempt(user, req.body);
                res.send(user);
              } else {
                res.status(500).send({error: err});
              }
            });
          } else {
            res.status(401).send({error: "Password was incorrect"});
          }
        } else {
          var user = new userModel({
            email: req.body.email,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            password: pHasher.generate(req.body.password),
            attempts: [
              {
                startTime: req.body.startTime,
                endTime: req.body.endTime
              }
            ]
          });

          user.save(function(err){
            if (!err){
              sendSlackAttempt(user, req.body);
              res.send(user);
            } else {
              res.status(500).send({error: err});
            }
          })
        }
      } else {
        res.status(500).send({error: err});
      }
    });
  } else {
    res.status(401).send({error: "You must enter an email and password"});
  }
});

// Serve static website
app.use(express.static('public'));
app.use('/bower_components', express.static('bower_components'));
// Use API router
app.use('/api', apiRouter)

app.listen(port);
console.log("Server started on port: " + port);
